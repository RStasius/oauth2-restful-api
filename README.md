Tai yra MVP projektas (forumas), kurio metu tikslas buvo sukurti OAUTH2 prisijungimo būdą bei pilnai veikiančią RESTFUL API sistemą.
Projektui atlikti buvo naudojami laravel (backend) + vue.js (frontend). Projektui buvo naudojamas laravel passport (reikalingas OAUTH2 autentifikacijai). 

Projekto paleidimo instrukcija:


1. Git clone
2. Composer install
3. Laravel passport integravimas

	* php artisan migrate
	* php artisan passport:install
	* php artisan passport:keys
	
	
4. Php artisan db:seed. Seedinimo metu bus sukurtas vienas useris(testas@testas.com, slaptažodis: test), 50 forumo temų, bei paskutinei temai 5 postai priskirti.
5. Kadangi visi postai ir temos yra priskirtos būtent testiniam vartotojui, jūs galėsite redaguoti ir trinti visas temas ar postus. Jeigu sukursite naują vartotoją, 
tų pačių postų ar temų editinti ar trinti nebebus galima.
6. Tikiuosi pavyko pasileisti projektą!

Projekto autorius Rytis Stasius.