<?php

use Illuminate\Database\Seeder;
use App\Post;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 0; $i < 5; $i++)
        {
            $post = new Post();
            $post->body= "Cia yra forumo vartotojo komentaras $i";
            $post->user_id = "1";
            $post->topic_id = "50";
            $post->save();
        }
    }
}
