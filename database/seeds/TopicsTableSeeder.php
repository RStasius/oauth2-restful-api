<?php

use App\Topic;
use Illuminate\Database\Seeder;

class TopicsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 0; $i < 50; $i++)
        {
            $topic = new Topic();
            $topic->title= "TemosPavadinimas $i";
            $topic->about = "Si tema yra apie medziokles ypatumus naktį $i";
            $topic->user_id = "1";
            $topic->save();
        }
    }
}
