
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import router from './routes.js';

import VueRouter from 'vue-router';
import Auth from './auth.js';

import http from 'axios'

require('./bootstrap');

window.Vue = require('vue');
window.auth = new Auth();

Vue.use(VueRouter);

window.Event = new Vue;




/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.component('navbar', require('./components/Navbar.vue'));
Vue.component('login', require('./components/Login.vue'));
Vue.component('topics', require('./components/Topics.vue'));
Vue.component('posts', require('./components/Posts.vue'));


const app = new Vue({
    el: '#app',

    router
});
