import VueRouter from 'vue-router';

let routes = [
    {
        path: '/',
        component: require('./components/Home.vue')
    },
    {
        path: '/topics',
        component: require('./components/Topics.vue'),
        meta: { middlewareAuth: true }
    },
    {
        path: '/login',
        component: require('./components/Login.vue')
    },
    {
        path: '/posts/:id',
        name: 'Posts',
        component: require('./components/Posts.vue'),
        props: true,
        meta: { middlewareAuth: true }
    },
    {
        path: '/register',
        component: require('./components/Register.vue')
    }
];

const router = new VueRouter({
    routes
});

router.beforeEach((to, from, next) => {

    if (to.matched.some(record => record.meta.middlewareAuth)) {
        if (!auth.check()) {
            next({
                path: '/login',
                query: { redirect: to.fullPath }
            });

            return;
        }
    }
    next();
});

export default router;