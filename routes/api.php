<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'LoginController@register');
Route::post('login', 'LoginController@login');

Route::middleware('auth:api')->group(function () {
    Route::post('logout', 'LoginController@logout');
    Route::get('get-user', 'LoginController@getUser');
    Route::get('topics','TopicController@index');
    Route::get('topics/{id}','TopicController@show');
    Route::post('topics','TopicController@store');
    Route::put('topics/{id}','TopicController@update');
    Route::delete('topics/{id}','TopicController@destroy');
    Route::get('topics/{id}/posts','TopicController@showTopicPosts')->name('posts');
    Route::get('topics/{id}/posts/{post_id}','TopicController@showTopicPost');

    Route::get('posts','PostController@index');
    Route::get('posts/{id}','PostController@show');
    Route::post('posts','PostController@store');
    Route::put('posts/{id}','PostController@update');
    Route::delete('posts/{id}','PostController@destroy');

});


Route::get('login','LoginController@index')->name('login');
Route::post('logout', 'LoginController@logout');