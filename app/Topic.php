<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    protected $table='topics';
    protected $fillable=['title','about'];
    protected $with = ['user','posts'];

    public function saveForumTopic($data)
    {
        $this->title = $data['title'];
        $this->about = $data['about'];
        $this->user_id= \Auth::user()->id;
        $this->save();
        return 1;
    }
    public function posts()
    {
        return $this->hasMany(Post::class)->orderBy('created_at','desc');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
