<?php

namespace App\Http\Controllers;

use App\Http\Resources\PostResource;
use Illuminate\Http\Request;
use App\Post;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::paginate(5);
        return PostResource::collection($posts);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = new Post();
        $post->id = $request->input('post_id');
        $post->body = $request->input('body');
        //dd($request);
        $post->topic_id = $request->input('topic_id');
        $post->user_id = \Auth::user()->id;

        if($post->save())
        {
            return new PostResource($post);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::findOrFail($id);

        if ($post->user_id != \Auth::user()->id) {
            return response()->json('Unauthorized.', 401);
        }
        else {
            return new PostResource($post);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $post = Post::findOrFail($id);

        if ($post->user_id != \Auth::user()->id) {
            return response()->json('Unauthorized.', 401);
        }
        else {
            Post::where('id', $id)->update($request->all());
            return new PostResource($post);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        $post = Post::findOrFail($id);

        if ($post->user_id != \Auth::user()->id) {
            return response()->json('Unauthorized.', 401);
        }
        else {
            if($post->delete())
            {
                return new PostResource($post);
            }
        }
    }
}
