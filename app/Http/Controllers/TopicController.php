<?php

namespace App\Http\Controllers;

use App\Http\Requests\TopicRequest;
use App\Http\Resources\PostResource;
use App\Http\Resources\TopicResource;
use App\Topic;
use Illuminate\Http\Request;
use App\Post;

class TopicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $topics = Topic::orderBy('created_at','desc')->paginate(5);
        return TopicResource::collection($topics);
    }

    public function showTopicPosts($id)
    {
        $topic = Topic::findOrFail($id);
        $posts = $topic->posts;

        return $posts;
    }

    public function topicPostsShow(Topic $topic)
    {
        return view('posts', compact('topic'));
    }

    public function showTopicPost($id, $post_id)
    {
        $topic = Topic::findOrFail($id);
        $post = $topic->posts()->where('id',$post_id)->firstOrFail();

        if ($post->user_id != \Auth::user()->id) {
            return response()->json('Unauthorized.', 401);
        }
        else {
            return $post;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(TopicRequest $request)
    {
        $topic = new Topic();
        $topic->id = $request->input('topic_id');
        $topic->title = $request->input('title');
        $topic->about = $request->input('about');
        $topic->user_id = \Auth::user()->id;

        if($topic->save())
        {
            return new TopicResource($topic);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        $topic = Topic::findOrFail($id);

        if ($topic->user_id != \Auth::user()->id) {
            return response()->json('Unauthorized.', 401);
        }
        else {
            return new TopicResource($topic);
        }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TopicRequest $request, $id)
    {
        $topic = Topic::findOrFail($id);

        if ($topic->user_id != \Auth::user()->id) {
            return response()->json('Unauthorized.', 401);
        }
        else {
            Topic::where('id', $id)->update($request->all());

            return new TopicResource($topic);
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        $topic = Topic::findOrFail($id);

        if ($topic->user_id != \Auth::user()->id) {
            return response()->json('Unauthorized.', 401);
        }
        else {

            if($topic->delete())
            {
                return new TopicResource($topic);
            }
        }
    }
}
