<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //protected $with = ['user', 'topic'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function topic()
    {
        return $this->belongsTo('App\Topic');
    }
}
